import java.util.List;

/**
 * Created by J on 29/11/2015.
 */
public class Car {

    private Orientation orientation;
    private String label;
    private List<PartOfCar> parts ;

    public PartOfCar getPart(int i){
        if(parts!=null && i<parts.size()){
            return parts.get(i);
        }
        return null;
    }

    public int getNumberOfParts(){

        return parts!=null? parts.size():0;
    }
}

