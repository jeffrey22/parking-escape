import java.util.List;

/**
 * Created by J on 29/11/2015.
 */
public class Top {

    private PartOfCar[][] grille;
    private Top ancestor;
    private int exitLine;
    private int exitCol;
    public Top(PartOfCar[][] grille) {
        this.grille = grille;
        ancestor=null;
    }

    public Top(List<Car> cars, int exitLine, int exitCol, Top ancestor) {

        this.grille= new PartOfCar[5][5];
        this.ancestor = ancestor;
        this.exitLine=exitLine;
        this.exitCol=exitCol;
        for(int i=0; i<cars.size();i++){
            Car currentCars=cars.get(i);
            for(int j=0;j< currentCars.getNumberOfParts();j++){
                PartOfCar part=currentCars.getPart(j);
                grille[part.getLigne()][part.getColonne()]=part;
            }
        }


    }

}
